export default class Json {

  async buscarOpcoes( idOpcoes ){
    return new Promise( resolve => {
    fetch( `http://localhost:3000/opcoes/${ idOpcoes }/` )
      .then( stringOpcao => stringOpcao.json() )
      .then( objetoOpcao => {
        resolve( objetoOpcao )
      } )
    } )
  }

  async buscarPorId( idSala ) {
    let resultado = []
    let urlIdSala = `http://localhost:3000/salas/${ idSala }/`
    return new Promise( resolve1 => {
      fetch(urlIdSala)
        .then( stringDaSala => stringDaSala.json() )
        .then( objetoSala => {
        
          let promissesOpc = objetoSala.idOpcoes.map( i => this.buscarOpcoes( i ) )
          Promise.all( promissesOpc ).then( arrayOpc => {
            resultado[0] = ( objetoSala )
            resultado[1] = ( arrayOpc )
            resolve1( resultado )
          } )
        } )
    } )
  }

  async buscarPorItem ( idArma ) {
    let urlIdItem = `http://localhost:3000/itens/${ idArma }/`
    return new Promise( resolve => {
      fetch( urlIdItem )
        .then( stringItem => stringItem.json() )
        .then( objetoItem => {
          resolve( objetoItem )
        } )
    } )
  }

  async buscarCadastros ( ) {
    let urlCadastros = `http://localhost:3000/cadastros/`
    return new Promise( resolve => {
      fetch( urlCadastros )
        .then( stringCadastros => stringCadastros.json() )
        .then( arrayCadastros => {
          resolve( arrayCadastros )
        } )
    } )
  }

  async usuarioUnico ( usuarioRecebido ) {
    let arrayCadastros = await this.buscarCadastros()
    for( let cadastroDoArray of arrayCadastros ) {  
      if( cadastroDoArray.usuario === usuarioRecebido  )
        return false
    }
    return true
  }

  async cadastroExiste ( usuarioRecebido, senhaRecebida ) {
    let arrayCadastros = await this.buscarCadastros()
    for( let cadastroDoArray of arrayCadastros ) {
      if( cadastroDoArray.usuario === usuarioRecebido && cadastroDoArray.senha === senhaRecebida )
        return true
    }
    return false
  }
  
}
