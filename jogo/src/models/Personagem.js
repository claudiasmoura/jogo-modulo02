export default class Personagem {
  constructor (nome) {
    this.nome = nome
    this.inventario = []
  }

  temItemPorId (id) {
    for(var item of this.inventario){
      if(item.id === id)
        return true
    }
    return false
  }

  temItem (idItem, nome, dano) {
    let itemProcurado = {
      id: idItem,
      nome: nome,
      dano: dano
    }
    return this.inventario.constains(itemProcurado)
  }

  adicionarItem (item) {
    this.inventario.push(item)
  }
}
