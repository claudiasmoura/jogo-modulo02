import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'

import App from './App.vue'
import Login from './components/screens/Login.vue'
import Cadastro from './components/screens/Cadastro.vue'
import Inicio from './components/screens/Inicio.vue'
import TelaJogo from './components/screens/TelaJogo.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VeeValidate)

const routes = [
  { name: 'login', path: '/', component: Login },
  { name: 'cadastro', path: '/cadastro', component: Cadastro},
  { name: 'inicio', path: '/inicio', component: Inicio },
  { name: 'jogo', path: '/jogo', component: TelaJogo }
]

const router = new VueRouter ({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
